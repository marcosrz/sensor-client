export const c2 = (x) => {
  
  const n = x.length;

  const mask = new Array(n).fill(1).join('');
  
  return ~((parseInt(x, 2) ^ parseInt(mask, 2)))
}

export default { c2 };
