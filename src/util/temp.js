import { c2 } from '../util/binary';

export const parseTemp = (buf) => {

  const SENSOR_RESOLUTION = 0.125;

  const { data } = buf.toJSON();

  const [msString, lsString] = data;

  const msa = msString.toString(2).split('').map(bit => parseInt(bit))
  const lsa = lsString.toString(2).split('').map(bit => parseInt(bit)).slice(0, 3);

  // fill msb with zeros
  while (msa.length < 8) {
    msa.unshift(0);
  }

  // fill lsb with zeros at left
  while (lsa.length < 3) {
    lsa.unshift(0);
  }

  // 11 bit word
  const word = [...msa, ...lsa];

  // Remove sign bit
  const sign = msa[0];

  let value;

  if (sign){
    value = -1 * parseInt(c2(word.join('')), 2);
  } else {
    value = parseInt(word.join(''), 2);
  }

  return value * SENSOR_RESOLUTION;

}

export default { parseTemp };
