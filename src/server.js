import config from './configure';
import publishingService from './services/publishing.service';
import lm74aService from './services/lm74a.service';

const { SENSOR_ID, UNIT, AVG_BALANCE_ITERATIONS } = process.env;

console.log('Config:', SENSOR_ID, UNIT);

const run = async () => {

  console.log('Reading sensor..');
  
  try {
  
    const sensorData = await getAvgBalancedTemperature(AVG_BALANCE_ITERATIONS);

    console.log('Posting data..');
  
    const { raw, value } = sensorData;

    const res = await publishingService.publish(SENSOR_ID, { unit: UNIT, raw, value });

    console.log('Reading published successfuly!', res.data);
  
  } catch (err) {
    console.error(err);
  }

}

const getAvgBalancedTemperature = async (iterations) => {

  const raws = [];

  for (let i = 0; i < iterations; i++) {

    const raw = await lm74aService.readTemp();

    raws.push(raw);
  }

  const value = raws.map(r => r.value).reduce((x,i) => x + i) / iterations;

  return { unit: UNIT, raw: value, value };

}

run();



