import { init } from 'raspi';
import { I2C } from 'raspi-i2c';
import { parseTemp } from '../util/temp';

export const readTemp = () => {

	return new Promise((resolve, reject) => {
		
		const wire = new I2C();

	        const buf = wire.readSync(0x48, 0, 2);

        	const temp = parseTemp(buf);

		resolve({ raw: temp, value: temp });
	})

}

export default { readTemp };
