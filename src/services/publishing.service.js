import axiosSensors from '../axios-sensors';

export const publish = (sensorId, data) => {

  return axiosSensors.post(`/${sensorId}/readings`, data)

}

export default { publish };
